// libNPBench.h
/* Library for Benchmarking

Copyright (C) 2016  Nikolaos Papadakis

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _LIBNPBENCH_h
#define _LIBNPBENCH_h

#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

class npBenchMark
{
 protected:
	bool microSTimeUnits;
    long unsigned (*timeFunc)();
    unsigned long  currentTime;
    unsigned long  lastTime;
    unsigned long  dt;
    unsigned long counts;
    unsigned long countsToPrint;
    char strBuffer[50]; // Char buffer to prepare string
    char strDbl[10];
    double dtAv ;
    virtual void printResult();
    virtual void varUpdates();
    virtual void resetVars();
 public:
    npBenchMark();
    npBenchMark(unsigned long maxCounts);
    npBenchMark(unsigned long maxCounts, bool vmicros);
    // Update Functions
    virtual void update();
};



#endif
