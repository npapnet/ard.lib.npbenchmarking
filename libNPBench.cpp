//
//
/* Library for Benchmarking

 Copyright (C) 2016  Nikolaos Papadakis

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "libNPBench.h"
/**
 * vmicros: true  if using micros() or
 *          false if using millis() [-default]
 */
npBenchMark::npBenchMark(unsigned long maxCounts, bool vmicros) {
	microSTimeUnits = vmicros;
	if (vmicros) {
		timeFunc = &micros;
	} else {
		timeFunc = &millis;
	}
	currentTime = timeFunc();
	lastTime = currentTime;
	dt = 0;
	dtAv = 0;
	counts = 0;
	countsToPrint = maxCounts;
}

npBenchMark::npBenchMark(unsigned long maxCounts) :
		npBenchMark::npBenchMark(maxCounts, false) {
}
npBenchMark::npBenchMark() :
		npBenchMark::npBenchMark(1000, false) {
}
;

/**
 Update function
 */
void npBenchMark::update() {
	// Benchmark time
	this->varUpdates();
	if (counts > countsToPrint) {
		printResult();
		resetVars();
	}
}

/**
 Function for updating base variables
 */
void npBenchMark::varUpdates() {
	// Benchmark time
	lastTime = currentTime;
	currentTime = timeFunc();
	dt = currentTime - lastTime;
	dtAv = (dtAv * counts + dt) / (counts + 1);
	counts += 1;
}

/**
 Function for Printing Result
 */
void npBenchMark::printResult() {
	// Arduino Code (see http://stackoverflow.com/questions/27651012/arduino-sprintf-float-not-formatting)
	dtostrf(dtAv, 4, 3, strDbl);
	sprintf(strBuffer, "Average [%cs]: %s \t Counts:%lu", (microSTimeUnits? 'u':'m' ), strDbl, counts);
	// proper C code
	// sprintf(strBuffer, "Average [%cs]: %f \t Counts:%lu", (microSTimeUnits? 'u':'m' ), dtAv, counts);
	Serial.print(strBuffer);
	Serial.println();
}
/*
 Function for resetting to zero variables
 */
void npBenchMark::resetVars() {
	counts = 0;
	dtAv = 0;
}
