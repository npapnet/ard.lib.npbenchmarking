/*
* This is an arduino sketch which uses an external Benchmarking 
*/

#include "libNPBenchMaxMin.h"

//npBenchMark *bm1= new npBenchMark() ;
npBenchMarkMaxMin *bm1= new npBenchMarkMaxMin() ;
npBenchMarkMaxMin *bm2= new npBenchMarkMaxMin(100) ;
npBenchMarkMaxMin *bm3= new npBenchMarkMaxMin(1000,true) ;

void setup() {
  // initialize serial and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println(F("Program for testing Benchmarking library"));
  Serial.println(F("----------------------------------------"));
  
}

void loop() {
   bm1->update();
   bm2->update();
   bm3->update();
   delay(5);
}
