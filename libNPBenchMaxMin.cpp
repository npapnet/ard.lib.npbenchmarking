//
/* Library for Benchmarking

Copyright (C) 2016  Nikolaos Papadakis

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "libNPBenchMaxMin.h"

/**
Update function
*/
void npBenchMarkMaxMin::update()
{
  // Benchmark time
   npBenchMark::update();
    if(counts > countsToPrint)
  {
    printResult();
    resetVars();
  }
}

void npBenchMarkMaxMin::printResult()
{
  npBenchMark::printResult();

  sprintf (strBuffer, "   Max: %lu \t Min: %lu", dtMax, dtMin);
  Serial.print( strBuffer );
  Serial.println();
}
/**
Function for Printing Result
*/
void npBenchMarkMaxMin::varUpdates()
{
  npBenchMark::varUpdates();
  if(counts<=1)
  {
    dtMax = dt;
    dtMin = dt;
  } else
  {
    dtMax = max(dtMax, dt);
    dtMin = min(dtMin, dt);
  }
}
/**
Function for resetting to zero variables
*/
void npBenchMarkMaxMin::resetVars()
{
    npBenchMark::resetVars();
}
