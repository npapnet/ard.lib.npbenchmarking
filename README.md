# Arduino Benchmarking lib and example

## Scope
To develop a simple loop Benchmarking library and test c++ inheritance

Two classes are created

- npBenchMark: outputs ony Time average 

- npBenchMarkMaxMin: outputs ony Time and Max Min
 
 
## REFs

- [Arduino.cc:: millis](https://www.arduino.cc/en/Reference/Millis)

- [Arduino.cc:: micros](https://www.arduino.cc/en/Reference/Micros)

## TODO


## History
2016-04-27:
- Changed From _object instance_ (init() method) to _pointer to Object_ (constructor() ). Major changes include:

   - __npBenchMark bm1;__   became __npBenchMark *bm1 = new npBenchMark();__ 

   - __bm1.update();__   became __bm1->update()__;
   
   See article in [What is the difference between __::__ __.__ and __->__ in c++ [duplicate]](http://stackoverflow.com/questions/11902791/what-is-the-difference-between-and-in-c)
  
   
- renamed npBenchMarkMaxMinClass -> npBenchMarkMaxMin

- renamed npBenchMarkClass -> npBenchMark

- remove shannanigan at end of class declaration _(npBenchMarkClass npBenchMark)_
    - [When to use Extern in C++](http://stackoverflow.com/questions/10422034/when-to-use-extern-in-c) used for global variables   

- Create option for micros instead of millis()

- removed printResultFlag (unnecessary flags)

- Created README.md

2016-04-26:

- Created test sketch __Ard-BenchmarkingLib.ino__

- Created  npBenchMarkClassMaxMin

- Created  npBenchMarkClass



